#!/bin/bash
#filternew.sh
cat $1 | grep "Deny" | awk '{print $11 "->" $13;}' | cut -f 1 -d '/' > filter.src
cat $1 | grep "Deny" | awk '{print $11 "->" $13;}' | cut -f 2 -d '>' > filter.dst
paste filter.src filter.dst > filter
sort filter | uniq -c > filtered
sort -rn filtered > result-srx
rm --interactive=never filter filtered filter.src filter.dst
